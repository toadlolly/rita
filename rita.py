#!/usr/bin/env python
from __future__ import print_function

import os
import sys
import praw
import re
import requests
import time
import logging
from contextlib import contextmanager

REDDIT_USERNAME = 'calcolatore'

USER_AGENT = ("Rita 0.3 for /r/italy by /u/nofunallowed98765")
SITES = [
    re.compile('.+corriere\.it.*'),
    re.compile('.+beppegrillo\.it.*'),
    re.compile('.+ilmanifesto\.info.*'),
    re.compile('.+ansa\.it.*')
]

SUBREDDIT = 'italy'
POST_LIMIT = 10
REFRESH_TIME = 600

MESSAGE = '''Per chi naviga da cellulare: [archive.today]({url}) | [text-only](http://textise.net/showText.aspx?strURL={post_url})

^^[Sorgente](https://gitlab.com/toadlolly/rita) ^^| ^^Ciao, ^^sono ^^un ^^bot. ^^Per ^^lamentele, ^^messaggia ^^[nofunallowed98765](https://www.reddit.com/user/nofunallowed98765/)[.](https://i.imgur.com/L67V6H4.gif)'''

@contextmanager
def login(username, password, user_agent):
    session = praw.Reddit(user_agent = USER_AGENT)
    try:
        session.login(username = REDDIT_USERNAME, password = REDDIT_PASSWORD)
        yield session
    finally:
        session.clear_authentication()

def archive(url):
    r = requests.post('https://archive.today/submit/', {'url' : url, 'coo' : ""})
    return r.headers['refresh'][6:]

# Return True if we haven't already mirrored the ID
# else return False
def check_id(id):
    with open('redditid.txt', 'r') as f:
        return not (id in f)
        
# Double Check that we haven't already posted here
# By searching for the bot name in the comments
def double_check(comments):
    for comment in comments:
        if comment.author.name == REDDIT_USERNAME:
            return False
    return True

def write_id(id):
    with open('redditid.txt', 'a') as f:
        f.write(id + "\n")

def do_magic():
    with login(REDDIT_USERNAME, REDDIT_PASSWORD, USER_AGENT) as session:
        sub = session.get_subreddit(SUBREDDIT)
        posts = sub.get_new(limit = POST_LIMIT)
        for post in posts:
            if any(regex.match(post.url) for regex in SITES):
                if check_id(post.id) and double_check(post.comments):
                    archive_url = archive(post.url)
                    post.add_comment(MESSAGE.format(url=archive_url, post_url=post.url))
                    logging.info(post.id + " - " + post.url)
                    write_id(post.id)
              
if __name__ == "__main__":
    logging.basicConfig(filename='rita.log'
                        ,level=logging.INFO
                        ,format='[%(asctime)s] - %(levelname)s - %(message)s')
    
    try:
        REDDIT_PASSWORD = os.environ['REDDIT_PASSWORD']
    except KeyError:
        logging.critical('No password configured, exiting...')
        sys.exit(1)

    while True:
        try:
            do_magic()
        except Exception, e:
            logging.exception(e)
        time.sleep(REFRESH_TIME)
